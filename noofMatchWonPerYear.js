const delivery = require ('./delivery.json');
const matches = require ('./matches.json');

const getAll = ((matches) => {
    const result = matches.reduce ((acc, val) => {
        if (!acc[val.winner]) {
            acc[val.winner] = {};
            acc[val.winner][val.season] = 1;
        }else {
            if (!acc[val.winner][val.season]) {
                acc[val.winner][val.season] = 1;
            }else {
                acc[val.winner][val.season]++;
            }
        }
        return acc;
    }, {})
    console.log(result);
})

getAll (matches);
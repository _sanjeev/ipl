const delivery = require('./delivery.json');
const matches = require('./matches.json');
const fs = require('fs');

const getData = ((matches, delivery) => {
    const result = delivery.map((key) => {
        const out = matches.find((val) => {
            if (key.match_id === val.id) {
                return true;
            }
        })

        key['season'] = out.season;
        return key;
    })
    // const output = result.filter ((key) => {
    //     return key.season === '2016';
    // })

    const finalOutput = result.reduce((acc, val) => {
        if (val.season === '2016') {
            if (!acc[val.batting_team]) {
                acc[val.batting_team] = {};
                acc[val.batting_team][val.season] = Number(val.extra_runs);
            } else {
                if (!acc[val.batting_team][val.season]) {
                    acc[val.batting_team][val.season] = Number(val.extra_runs);
                } else {
                    acc[val.batting_team][val.season] += Number(val.extra_runs);
                }
            }
        }
        return acc;
    }, {})
    console.log(finalOutput);
})

getData(matches, delivery);
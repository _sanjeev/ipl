const delivery = require('./delivery.json');
const matches = require('./matches.json');

const top10economy = ((matches, delivery) => {
    const result = delivery.map((val) => {
        const res = matches.find((key) => {
            return key.id === val.match_id;
        })
        val['season'] = res.season;
        return val;
    })
    // const res = result.filter ((key) => {
    //     return key.season === '2015';
    // })
    const output = result.reduce((acc, val) => {
        if (val.season === '2015') {
            if (!acc[val.bowler]) {
                acc[val.bowler] = {};
                acc[val.bowler][val.season] = [];
                acc[val.bowler][val.season][0] = Number(val.total_runs);
                acc[val.bowler][val.season][1] = 1;
            } else {
                if (!acc[val.bowler][val.season]) {
                    acc[val.bowler][val.season] = [];
                    acc[val.bowler][val.season][0] = Number(val.total_runs);
                    acc[val.bowler][val.season][1] = 1;
                } else {
                    // acc[val.bowler][val.season] = [];
                    acc[val.bowler][val.season][0] += Number(val.total_runs);
                    acc[val.bowler][val.season][1] += 1;
                }

            }

        }
        return acc;
    }, {})
    // console.log(output);

    const res = Object.entries(output).map((key) => {
        const op = Object.entries(key[1]).reduce((acc, val) => {
            let totalRun = val[1][0];
            let totalOver = Math.floor(val[1][1] / 6) + ((val[1][1] % 6) * 0.1);
            acc[val[0]] = (totalRun / totalOver).toFixed(2);
            return acc;
        }, {})
        // console.log(op);
        key[1] = op;
        return key;

    })
    //console.log(res);
    const resultant = res.map((key) => {
        key[1] = Object.entries(key[1]);
        return key;
    })
    console.log(Object.fromEntries(resultant.sort((a, b) => {
        return a[1][0][1] - b[1][0][1];
    }).map((key) => {
        key[1] = Object.fromEntries (key[1]);
        return key;
    })));
    resultant[1].sort((a, b) => {

    })


})

top10economy(matches, delivery);